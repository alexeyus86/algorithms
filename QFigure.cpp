#include "QFigure.h"

QFigure::QFigure(bool isCanResize) :isCanResize(isCanResize){
};

void
QFigure::mouseDoubleClick(QMouseEvent *event) {

    qDebug() << "mouseDoubleClick: enter";
//    emit mouseDoubleClicked();

//    QObject* obj = sender();
//    QCustomPlot *figure = qobject_cast<QCustomPlot *>(obj);

//    if (event->button() == Qt::RightButton){
        this->rescaleAxes(true);
        this->replot(QCustomPlot::rpQueuedReplot);
//    }
    qDebug() << "mouseDoubleClick: exit";
}

void
QFigure::mousePress(QMouseEvent *event) {
    qDebug() << "mousePress: enter";
    if (event->button() == Qt::LeftButton) {
        qDebug() << this->xAxis->pixelToCoord(event->pos().x());
        qDebug() << this->yAxis->pixelToCoord(event->pos().y());
        xRange_p2.first = this->xAxis->pixelToCoord(event->pos().x());
        yRange_p2.first = this->yAxis->pixelToCoord(event->pos().y());
    }
    qDebug() << "mousePress: exit";
}

void
QFigure::mouseRelease(QMouseEvent *event) {
    qDebug() << "mouseRelease: enter";
    if (event->button() == Qt::LeftButton) {
        qDebug() << this->xAxis->pixelToCoord(event->pos().x());
        qDebug() << this->yAxis->pixelToCoord(event->pos().y());
        xRange_p2.second = this->xAxis->pixelToCoord(event->pos().x());
        yRange_p2.second = this->yAxis->pixelToCoord(event->pos().y());

        if (isCanResize)
        if (xRange_p2.first != xRange_p2.second){
            this->xAxis->setRange(xRange_p2.first, xRange_p2.second);
            this->yAxis->setRange(yRange_p2.first, yRange_p2.second);
            this->replot(QCustomPlot::rpQueuedReplot);
        }
        emit mouseRelease();
    }
    qDebug() << "mouseRelease: exit";
}

void
QFigure::clearAt(int index) {
    this->graph(index)->data().data()->clear();
    this->replot(QCustomPlot::rpQueuedReplot);
}

void
QFigure::clearAll() {
    for (auto i=0; i<this->graphCount(); i++){
        this->graph(i)->data().data()->clear();
    }
    this->replot(QCustomPlot::rpQueuedReplot);
}
