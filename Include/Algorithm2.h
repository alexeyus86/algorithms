#ifndef ALGORITHMS_ALGORITHM2_H
#define ALGORITHMS_ALGORITHM2_H

//#include <QVector>
#include "Common.h"
//
class Algorithm2 {
public:
    Algorithm2();
    ~Algorithm2();
    double Fs = 0;
    QVector<double> t;
    QVector<double> u;
public:
    void Init(const boost::math::cubic_b_spline<double> &, const QVector<double> &, const QVector<double> &);
    void Reset();
    void Update();
};


#endif //ALGORITHMS_ALGORITHM2_H
