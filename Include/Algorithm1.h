#ifndef ALGORITHMS_ALGORITHM1_H
#define ALGORITHMS_ALGORITHM1_H

#include "Include/Common.h"

class Algorithm1 {
public:
    Algorithm1();
    ~Algorithm1();
    double Fs = 0;
    QVector<double> t;
    QVector<double> u;
public:
    void Init(const boost::math::cubic_b_spline<double> &, const QVector<double> &, const QVector<double> &);
    void Reset();
    void Update();
};


#endif //ALGORITHMS_ALGORITHM1_H
