#ifndef FIGURE_CUSTOM_H
#define FIGURE_CUSTOM_H

#include <QObject>
#include <QWidget>
#include <QFrame>
#include "qcustomplot.h"

class QFigure : public QCustomPlot {
Q_OBJECT
public:
    bool isCanResize = true;
    std::pair<double, double> xRange_p = {0, 0};
    std::pair<double, double> yRange_p = {0, 0};
    std::pair<double, double> xRange_p2 = {0, 0};
    std::pair<double, double> yRange_p2 = {0, 0};
public:
    explicit QFigure(bool isCanResize = true);
    ~QFigure() override = default;
    void clearAt(int);
    void clearAll();

signals:
    void mouseRelease();

private slots:
    void mouseDoubleClick(QMouseEvent *event) override;
    void mousePress(QMouseEvent *event) override;
    void mouseRelease(QMouseEvent *event) override;
};

#endif // FIGURE_CUSTOM_H
