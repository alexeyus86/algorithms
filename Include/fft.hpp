//.h file code:

#ifndef FTT_H
#define FTT_H

#define _USE_MATH_DEFINES
//#include <vector>
//#include <cmath>
//#include <stdexcept>
#include <complex>
#include <algorithm>

//#include <bits/stl_algobase.h>

//#include <omp.h>

/*
 * Free FFT and convolution (C#)
 *
 * Copyright (c) 2017 Project Nayuki. (MIT License)
 * https://www.nayuki.io/page/free-small-fft-in-multiple-languages
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * - The above copyright notice and this permission notice shall be included in
 *   all copies or substantial portions of the Software.
 * - The Software is provided "as is", without warranty of any kind, express or
 *   implied, including but not limited to the warranties of merchantability,
 *   fitness for a particular purpose and noninfringement. In no event shall the
 *   authors or copyright holders be liable for any claim, damages or other
 *   liability, whether in an action of contract, tort or otherwise, arising from,
 *   out of or in connection with the Software or the use or other dealings in the
 *   Software.
 */

template<typename T, typename C>
class FFT {
    /*
     * Computes the discrete Fourier transform (DFT) or inverse transform of the given complex vector, storing the result back into the vector.
     * The vector can have any length. This is a wrapper function. The inverse transform does not perform scaling, so it is not a true inverse.
     */
public:

    static std::pair<T, C> rms_fft(const T &x, const C &y);

    static C rms_ifft(const T &x, const C &y);

    static std::pair<T, C> rms_fft(const T &x, const T &y);

    static C rms_ifft(const T &x, const T &y);

    static void Transform(C &vector, bool inverse);

    /*
     * Computes the discrete Fourier transform (DFT) of the given complex vector, storing the result back into the vector.
     * The vector's length must be a power of 2. Uses the Cooley-Tukey decimation-in-time radix-2 algorithm.
     */
    static void TransformRadix2(C &vector, bool inverse);

    /*
     * Computes the discrete Fourier transform (DFT) of the given complex vector, storing the result back into the vector.
     * The vector can have any length. This requires the convolution function, which in turn requires the radix-2 FFT function.
     * Uses Bluestein's chirp z-transform algorithm.
     */
    static void TransformBluestein(C &vector, bool inverse);

    /*
     * Computes the circular convolution of the given complex vectors. Each vector's length must be the same.
     */
    static void Convolve(C &xvector, C &yvector, C &outvector);

    static T Conv(const T &, const T &);

private:

    static int ReverseBits(int val);
};

template<typename T, typename C>
std::pair<T, C> FFT<T, C>::rms_fft(const T &x, const C &y) {
    auto n = x.size();

    T out_x(n);
    C out_y = y;

    Transform(out_y, false);

    double FS = 1.0 / qAbs(x[1] - x[0]);
    double delta = FS / (n - 1);
    for (int i = 0; i < n; i++) {
        out_x[i] = i * delta;
        out_y[i] /= FS;
    }

    return out(out_x, out_y);
}

template<typename T, typename C>
C FFT<T, C>::rms_ifft(const T &x, const C &y) {
    auto n = y.size();
    C out(n);
    for (int i = 0; i < n; i++) {
        out[i] = y[i];
    }

    Transform(out, true);

    double FS = x.back();
    for (int i = 0; i < n; i++) {
        out[i] *= FS;
    }

    return out;
}

template<typename T, typename C>
std::pair<T, C> FFT<T, C>::rms_fft(const T &x, const T &y) {
    auto n = x.size();

    std::pair<T, C> out;

    out.first.resize(n);
    out.second.resize(n);

    for (int i = 0; i < n; i++) {
        out.second[i] = y[i];
    }

    Transform(out.second, false);

    double FS = 1.0 / qAbs(x[1] - x[0]);
    double delta = FS / (n - 1);
    for (int i = 0; i < n; i++) {
        out.first[i] = i * delta;
        out.second[i] /= FS;
    }

    return out;
}

//template <typename T, typename C>
//std::pair<T, C> FFT<T, C>::rms_ifft(T x, T y){
//    auto n = y.size();
//    std::pair<T, C> out;
//    out.first.resize(n);
//    out.second.resize(n);
//    for (int i = 0; i < n; i++)
//        out.second[i] = y[i];
//    Transform(out.second, true);
//    double FS = 1.0/(x[3] - x[2]);
//    for (int i = 0; i < n; i++){
//        out.first[i] = i * FS / n;
//        out.second[i] /= FS;}
//    return out;
//}

template<typename T, typename C>
C FFT<T, C>::rms_ifft(const T &x, const T &y) {
    auto n = y.size();
    C out(n);

    for (int i = 0; i < n; i++) {
        out[i] = y[i];
    }

    Transform(out, true);

    double FS = x.back();

    for (int i = 0; i < n; i++) {
        out[i] *= FS;
    }

//    for (C *o: out)
//        o *= FS;

    return out;
}

//template <typename T, typename C>
//std::pair<T, C> FFT<T, C>::rms_fft(T x, C y){

//    auto n = y.size();

//    T out_x(n);
//    C out_y = y;

//    Transform(out_y, false);

//    double FS = 1.0/(x[3] - x[2]);
//    for (int i = 0; i < n; i++){
//        out_x[i] = i * FS / n;
//        out_y[i] /= FS;
//    }

//    std::pair<T, C> out(out_x, out_y);

//    return out;
//}

//template <typename T, typename C>
//std::pair<T, C> FFT<T, C>::rms_ifft(T x, C y){

//    auto n = y.size();

//    T out_x(n);
//    C out_y = y;

//    Transform(out_y, true);

//    double FS = 1.0/(x[3] - x[2]);
//    for (int i = 0; i < n; i++){
//        out_x[i] = i * FS / n;
//        out_y[i] /= FS;
//    }

//    std::pair<T, C> out(out_x, out_y);

//    return out;
//}

//template <typename T, typename C>
//std::pair<T, C> FFT<T, C>::rms_fft(T x, T y){

//    auto n = y.size();

//    T out_x(n);
//    C out_y(n);

//    for (int i = 0; i < n; i++)
//        out_y[i] = y[i];

//    Transform(out_y, false);

//    double FS = 1.0/(x[3] - x[2]);
//    for (int i = 0; i < n; i++){
//        out_x[i] = i * FS / n;
//        out_y[i] /= FS;
//    }

//    std::pair<T, C> out(out_x, out_y);

//    return out;
//}

//template <typename T, typename C>
//std::pair<T, C> FFT<T, C>::rms_ifft(T x, T y){

//    auto n = y.size();

//    T out_x(n);
//    C out_y(n);

//    for (int i = 0; i < n; i++)
//        out_y[i] = y[i];

//    Transform(out_y, true);

//    double FS = 1.0/(x[3] - x[2]);
//    for (int i = 0; i < n; i++){
//        out_x[i] = i * FS / n;
//        out_y[i] /= FS;
//    }

//    std::pair<T, C> out(out_x, out_y);

//    return out;
//}

template<typename T, typename C>
void FFT<T, C>::Transform(C &vector, bool inverse) {
    int n = vector.size();
    if (n == 0) {
        return;
    } else if ((n & (n - 1)) == 0) // Is power of 2
    {
        TransformRadix2(vector, inverse);
    } else // More complicated algorithm for arbitrary sizes
    {
        TransformBluestein(vector, inverse);
    }
}

template<typename T, typename C>
void FFT<T, C>::TransformRadix2(C &vector, bool inverse) {
    // Length variables
    int n = vector.size();
    int levels = 0; // compute levels = floor(log2(n))
    for (int temp = n; temp > 1; temp >>= 1) {
        levels++;
    }
    if (1 << levels != n) {
        throw std::invalid_argument("Length is not a power of 2");
    }

    // Trigonometric table
    C expTable(n / 2);
    double coef = 2 * M_PI / n * (inverse ? 1 : -1);
//#pragma omp parallel for
    for (int i = 0; i < n / 2; i++) {
        expTable[i] = std::exp(std::complex<double>(0, i * coef));
    }

    // Bit-reversed addressing permutation
    for (int i = 0; i < n; i++) {
        int j = static_cast<int>(static_cast<unsigned int>(ReverseBits(i)) >> (32 - levels));
        if (j > i) {
            std::complex<double> temp = vector[i];
            vector[i] = vector[j];
            vector[j] = temp;
        }
    }

    // Cooley-Tukey decimation-in-time radix-2 FFT<T>
    for (int size = 2; size <= n; size *= 2) {
        int halfsize = size / 2;
        int tablestep = n / size;
        for (int i = 0; i < n; i += size) {
            for (int j = i, k = 0; j < i + halfsize; j++, k += tablestep) {
                std::complex<double> temp = vector[j + halfsize] * expTable[k];
                vector[j + halfsize] = vector[j] - temp;
                vector[j] += temp;
            }
        }
        if (size == n) // Prevent overflow in 'size *= 2'
        {
            break;
        }
    }
}

template<typename T, typename C>
void FFT<T, C>::TransformBluestein(C &vector, bool inverse) {
    // Find a power-of-2 convolution length m such that m >= n * 2 + 1
    int n = vector.size();
    if (n >= 0x20000000) {
        throw std::invalid_argument("Array too large");
    }
    int m = 1;
    while (m < n * 2 + 1) {
        m *= 2;
    }

    // Trignometric table
    C expTable(n);
    double coef = M_PI / n * (inverse ? 1 : -1);
//#pragma omp parallel for
    for (int i = 0; i < n; i++) {
        int j = static_cast<int>(static_cast<long long>(i) * i % (n * 2)); // This is more accurate than j = i * i
        expTable[i] = std::exp(std::complex<double>(0, j * coef));
    }

    // Temporary vectors and preprocessing
    C avector(m);
//#pragma omp parallel for
    for (int i = 0; i < n; i++) {
        avector[i] = vector[i] * expTable[i];
    }
    C bvector(m);
    bvector[0] = expTable[0];
    for (int i = 1; i < n; i++) {
        bvector[m - i] = std::conj(expTable[i]);
        bvector[i] = bvector[m - i];
    }

    // Convolution
    C cvector(m);
    Convolve(avector, bvector, cvector);

    // Postprocessing
//#pragma omp parallel for
    for (int i = 0; i < n; i++) {
        vector[i] = cvector[i] * expTable[i];
    }
}

template<typename T, typename C>
void FFT<T, C>::Convolve(C &xvector, C &yvector, C &outvector) {
    int n = xvector.size();
    if (n != yvector.size() || n != outvector.size()) {
        throw std::invalid_argument("Mismatched lengths");
    }
    Transform(xvector, false);
    Transform(yvector, false);
    for (int i = 0; i < n; i++) {
        xvector[i] *= yvector[i];
    }
    Transform(xvector, true);
    for (int i = 0; i < n; i++) // Scaling (because this FFT<T> implementation omits it)
    {
        outvector[i] = xvector[i] / std::complex<double>(n);
    }
}

template<typename T, typename C>
T FFT<T, C>::Conv(const T &a, const T &b) {
    auto n = a.size();
    T out(n);
    C out_c(n);
    C a_c(n);
    C b_c(n);

    for (int i = 0; i < n; ++i) {
        a_c[i] = a[i];
        b_c[i] = b[i];
    }

    Transform(a_c, false);
    Transform(b_c, false);

    for (int i = 0; i < n; ++i) {
        out_c[i] = a_c[i] * std::conj(b_c[i]);
    }

    Transform(out_c, true);

    for (int i = 0; i < n; ++i) {
        out[i] = std::real(out_c[i]);
    }

    return out;
}

template<typename T, typename C>
int FFT<T, C>::ReverseBits(int val) {
    int result = 0;

    for (int i = 0; i < 32; i++, val >>= 1) {
        result = (result << 1) | (val & 1);
    }
    return result;
}

#endif //FTT_H
