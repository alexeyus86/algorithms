#include "Algorithm3.h"

Algorithm3::Algorithm3(){

};

Algorithm3::~Algorithm3(){

}

void
Algorithm3::Init(const boost::math::cubic_b_spline<double> &spline, const QVector<double> &t0, const QVector<double> &u0) {
    auto n = t0.size();
    auto Fs0 = 1/t0[1];
    Fs = 30*3348;
    n *= Fs / Fs0;
    print(n);
    t.resize(n);
    u.resize(n);
    auto delta = 1.0/Fs;
    for (int i = 0; i < n; i++) {
        t[i] = i * delta;
        u[i] = spline(t[i]) * 1024;
    }
    print(this->Fs);
}


void
Algorithm3::Reset() {

}

void
Algorithm3::Update() {

}
