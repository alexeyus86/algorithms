#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QComboBox>
#include <QSpinBox>
#include <QLineEdit>
#include <QSlider>
#include <QLabel>
#include <QPlainTextEdit>
#include <QCheckBox>
#include <QSpacerItem>
#include <QFile>
#include <QDir>
#include <QFileInfo>
#include <QMimeData>

#include "qcustomplot.h"
#include "QFigure.h"

#include <QThread>
#include <time.h>

//#include "defineds.h"

#include <Common.h>

#include "Algorithm1.h"
#include "Algorithm2.h"
#include "Algorithm3.h"

using nl = std::numeric_limits<double>;

#include <boost/math/interpolators/cubic_b_spline.hpp>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QWidget *w;
    QHBoxLayout *mainLayout;
    QBoxLayout *leftLayout;
    QBoxLayout *rightLayout;
    QGroupBox *qGroupBox;
    QSpinBox *qSpinBox;
    QLineEdit *qLineEdit;
    QSlider *qSlider;
    QLabel *qLabel;
    QPushButton *qPushButtonPlayStop;
    QPushButton *qPushButtonReset;
    QPushButton *qPushButtonDecodeFrame;
    QPlainTextEdit *qPlainTextEdit;
    QCheckBox *qCheckBoxFilter;
    QCheckBox *qCheckBoxFFT;
    QSplitter *qSplitterTime;
    QSplitter *qSplitterFreq;

    QFigure *qCustomPlotTime1;
    QFigure *qCustomPlotTime2;
    QFigure *qCustomPlotFreq1;
    QFigure *qCustomPlotFreq2;

    QFile file;
    QDir dir;
    QString directory;

    qVect_1d t;
    qVect_1d u;

    qVect_1d t_frame;
    qVect_1d u_frame;

    qVect_1d t_plot;
    qVect_1d u_plot;

    AudioFile<double> *audioFile;

    int n_min = 0;
    int n_max = 0;

    QString lastFunctionCalled = "";

    Algorithm1 *algorithm1;
    Algorithm2 *algorithm2;
    Algorithm3 *algorithm3;

private slots:
    void fft(QVector<double> &, QVector<double> &);
    void qSliderReleased();
    void decodeFrame();
    void plotsUpdate();
    void reset();

private:
    void dropEvent(QDropEvent *event) override;
    void dragMoveEvent(QDragMoveEvent *event) override;
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dragLeaveEvent(QDragLeaveEvent *event) override;
};
#endif // MAINWINDOW_