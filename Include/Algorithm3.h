#ifndef ALGORITHMS_ALGORITHM3_H
#define ALGORITHMS_ALGORITHM3_H

//#include <QVector>
#include "Common.h"
//
class Algorithm3 {
public:
    Algorithm3();
    ~Algorithm3();
    double Fs = 0;
    QVector<double> t;
    QVector<double> u;
public:
    void Init(const boost::math::cubic_b_spline<double> &, const QVector<double> &, const QVector<double> &);
    void Reset();
    void Update();
};


#endif //ALGORITHMS_Algorithm3_H
