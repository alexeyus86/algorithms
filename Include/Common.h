#ifndef ALGORITHMS_COMMON_H
#define ALGORITHMS_COMMON_H

#include <QVector>
#include <QDebug>
#include <complex>

#define print(arg) (qDebug()<<#arg<<"="<<arg);

typedef QVector<double> qVect_1d;
typedef QVector<QVector<double>> qVect_2d;
typedef QVector<QVector<QVector<double>>> qVect_3d;
typedef QVector<std::complex<double>> qVect_1c;
typedef std::pair<qVect_1d, qVect_1d> qVect_p_1d_1d;
typedef std::pair<double, double> p_1d_1d;
typedef QList<double> qList_1d;
typedef QList<int> qList_1i;


#include "AudioFile.h"
#include <boost/math/interpolators/cubic_b_spline.hpp>

#endif //ALGORITHMS_COMMON_H
