#ifndef GT_H
#define GT_H

#include "defineds.h"

#if   N_FFT == 128
//const int32 coef_1 = 0;
//const int32 coef_2 = 1024;
#define coef_1 1024
#define coef_2 0
//#define coef_1 0
//#define coef_2 1024
#elif N_FFT == 256
//const int32 coef_1 = 724;
//const int32 coef_2 = 724;
#define coef_1 724
#define coef_2 724
#else
#define coef_1 0
#define coef_2 0
#endif

float calc_FRM(int16 data[], int16 n1, int16 n2)
{
    int32 Re = data[0];
    int32 Im = 0;
    int32 Re_p = 0;
    int32 Im_p = 0;
    int16 i = 0;
    for (i = n1; i < n2; ++i)
    {
        //#if   coef_1 == 0
        //        Re_p = Re;
        //        Im_p = Im;
        //        Re = (int32) data[i] - ((coef2) * (Im_p) >> 10);
        //        Im = ((coef2) * (Re_p) >> 10);
        //#elif coef_1 == coef_2
        //        Re_p = Re;
        //        Im_p = Im;
        //        Re = ((coef1) * (Re_p - Im_p) >> 10) + (int32) data[i];
        //        Im = ((coef1) * (Re_p + Im_p) >> 10);
        //#else
        Re_p = Re;
        Im_p = Im;
        Re = ((coef_1) * (Re_p) >> 10) - ((coef_2) * (Im_p) >> 10) + (int32)data[i];
        Im = ((coef_1) * (Im_p) >> 10) + ((coef_2) * (Re_p) >> 10);
        //#endif
    }
    float Im_r = Im / (Fs);
    float Re_r = Re / (Fs);
    //return (Uint16)(Re_r * Re_r + Im_r * Im_r);
    return (Re_r * Re_r + Im_r * Im_r);
}

#endif // GT_H
