#include "mainwindow.h"
#include <QDebug>
#include "fft.hpp"
#include <QThread>
#include <QProgressDialog>
#include <QProcess>

#include <thread>
#include <functional>

#define DEBUG 0

void func() {
}

enum Error {
    NoError,
    UserCanseledImportAudioFile,
    FileNameError
};

int error = 0;

MainWindow::MainWindow(QWidget *parent)
        : QMainWindow(parent) {
    qGroupBox = new QGroupBox();
    qSpinBox = new QSpinBox();
    qLineEdit = new QLineEdit();
    qSlider = new QSlider(Qt::Horizontal);
    qLabel = new QLabel();

    qCheckBoxFilter = new QCheckBox();
    qCheckBoxFilter->setText("Filtering");

    qCheckBoxFFT = new QCheckBox();
    qCheckBoxFFT->setText("FFT");

    qPushButtonPlayStop = new QPushButton();
    qPushButtonPlayStop->setText("Play");

    qPushButtonReset = new QPushButton();
    qPushButtonReset->setText("Reset");

    qPushButtonDecodeFrame = new QPushButton();
    qPushButtonDecodeFrame->setText("Decode frame");

    qPlainTextEdit = new QPlainTextEdit();
//    qPlainTextEdit->setMaximumSize(200, 800);
    qPlainTextEdit->setMaximumWidth(200);
    qPlainTextEdit->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
//    qPlainTextEdit->sizePolicy().setVerticalStretch(2);

    QBoxLayout *qGroupBoxLayout = new QVBoxLayout;
    QSpacerItem *qSpacerItemGroupBox = new QSpacerItem(50, 10, QSizePolicy::Minimum, QSizePolicy::Preferred);
    qGroupBoxLayout->addSpacerItem(qSpacerItemGroupBox);
    qGroupBoxLayout->addWidget(qCheckBoxFFT);
    qGroupBoxLayout->addWidget(qCheckBoxFilter);
    qGroupBox->setLayout(qGroupBoxLayout);

    auto *tmp = new QCustomPlot();

    qCustomPlotTime1 = new QFigure();
    qCustomPlotTime1->addGraph();
    qCustomPlotTime1->addGraph();
    qCustomPlotTime1->xAxis->setLabel("time, s");
    qCustomPlotTime1->yAxis->setLabel("Amplitude, mV");
    qCustomPlotTime1->replot();
    qCustomPlotTime2 = new QFigure(false);
    qCustomPlotTime2->addGraph();
    qCustomPlotTime2->xAxis->setLabel("time, s");
    qCustomPlotTime2->yAxis->setLabel("Amplitude, mV");
    qCustomPlotTime2->replot();
    qCustomPlotFreq1 = new QFigure();
    qCustomPlotFreq1->addGraph();
    qCustomPlotFreq2 = new QFigure();
    qCustomPlotFreq2->addGraph();

    qSplitterTime = new QSplitter();
    qSplitterTime->setHandleWidth(10);
    qSplitterTime->setFixedWidth(1);
    qSplitterTime->setStretchFactor(0, 1);
    qSplitterTime->setStretchFactor(1, 0);

    QPalette palette;
    palette.setColor(QPalette::Background, QColor(0, 0, 0, 5));
    qSplitterTime->setPalette(palette);

    qSplitterTime->addWidget(qCustomPlotTime1);
    qSplitterTime->addWidget(qCustomPlotTime2);

    QBoxLayout *timeLayout = new QHBoxLayout;
    timeLayout->addWidget(qSplitterTime);

    qSplitterFreq = new QSplitter();
    qSplitterFreq->setHandleWidth(10);
    qSplitterFreq->setFixedWidth(1);
    qSplitterFreq->setStretchFactor(0, 1);
    qSplitterFreq->setStretchFactor(1, 0);

    QPalette paletteFreq;
    palette.setColor(QPalette::Background, QColor(0, 0, 0, 5));
    qSplitterFreq->setPalette(paletteFreq);

    qSplitterFreq->addWidget(qCustomPlotFreq1);
    qSplitterFreq->addWidget(qCustomPlotFreq2);

    QBoxLayout *freqLayout = new QHBoxLayout;
    freqLayout->addWidget(qSplitterFreq);

    leftLayout = new QVBoxLayout;
    rightLayout = new QVBoxLayout;

    leftLayout->addLayout(timeLayout);
    leftLayout->addWidget(qSlider);
    leftLayout->addLayout(freqLayout);

    rightLayout->addWidget(qPlainTextEdit);
    rightLayout->addWidget(qGroupBox);
    rightLayout->addWidget(qPushButtonPlayStop);
    rightLayout->addWidget(qPushButtonDecodeFrame);
    rightLayout->addWidget(qPushButtonReset);

    mainLayout = new QHBoxLayout;
    mainLayout->addLayout(leftLayout);
    mainLayout->addLayout(rightLayout);

    auto *pQWidget = new QWidget();

    pQWidget->setMinimumSize(QSize(800, 600));
//    pQWidget->setMaximumSize(QSize(1200, 800));
    pQWidget->setLayout(mainLayout);
    pQWidget->setWindowTitle("qt20");

    setCentralWidget(pQWidget);
    resize(QSize(1200, 800));
    setAcceptDrops(true);

    algorithm1 = new Algorithm1();
    algorithm2 = new Algorithm2();
    algorithm3 = new Algorithm3();

#if DEBUG
    directory = "C:\\cross.wav";
    print(directory);
    bool loadedOK = audioFile.load(directory.toStdString());
    if (loadedOK) {
        int n = static_cast<int>(audioFile.samples[0].size());

        t.resize(n);
        u.resize(n);

        for (auto i = 0; i < n; ++i) {
            t[i] = (static_cast<double>(i)) / audioFile.getSampleRate();
            u[i] = audioFile.samples[0][i];
        }

        qDebug() << audioFile.getSampleRate();

        boost::math::cubic_b_spline<double> spline(u.begin(), u.end(), t[0], t[1] - t[0], 0, 0);

        n *= Fs / audioFile.getSampleRate();

        print(n);

        QVector<double> t_t(n);
        QVector<double> u_t(n);

        auto delta = (t.last() - t.first()) / (t_t.size() - 1);

        print(delta);

        for (int i = 0; i < n; i++) {
            t_t[i] = i * delta;
            u_t[i] = spline(t_t[i]) * 1024;
        }

        if (!qCustomPlotTime1->graphCount()) {
            qCustomPlotTime1->addGraph();
        } else {
            qCustomPlotTime1->graph(0)->data().data()->clear();
        }
        qCustomPlotTime1->graph(0)->setData(t_t, u_t);
        qCustomPlotTime1->rescaleAxes(true);
        qCustomPlotTime1->replot(QCustomPlot::rpQueuedReplot);

        t = t_t;
        u = u_t;

        qSlider->setRange(0, n);

        print(qSlider->maximum());
    }
#endif
//
    QProcess::execute("clearAt");
    QProcess::execute("CLS");

#if defined _WIN32
    system("cls");
#elif defined (__LINUX__) || defined(__gnu_linux__) || defined(__linux__)
    system("clear");
#elif defined (__APPLE__)
    system("clear");
#endif

    bool allWasConnectedOk = true;
    allWasConnectedOk = (allWasConnectedOk && connect(qPushButtonDecodeFrame, SIGNAL(released()), this, SLOT(decodeFrame())));
//    allWasConnectedOk = (allWasConnectedOk && connect(qCustomPlotTime1, SIGNAL(mouseQuadrupleClicked()), this, SLOT(plotsUpdate())));
    allWasConnectedOk = (allWasConnectedOk && connect(qCustomPlotTime1, SIGNAL(mouseRelease()), this, SLOT(plotsUpdate())));
    allWasConnectedOk = (allWasConnectedOk && connect(qPushButtonReset, SIGNAL(released()), this, SLOT(reset())));
    allWasConnectedOk = (allWasConnectedOk && connect(qSlider, SIGNAL(valueChanged(int)), this, SLOT(qSliderReleased())));
    Q_ASSERT(allWasConnectedOk);
}

MainWindow::~MainWindow()
= default;

void
MainWindow::dragEnterEvent(QDragEnterEvent *event) {
    qDebug() << "dragEnterEvent: enter";
    if (event->mimeData()->hasUrls()) {
        event->acceptProposedAction();
    }
    qDebug() << "dragEnterEvent: exit";
}

void
MainWindow::dropEvent(QDropEvent *event) {
    qDebug() << "dropEvent: enter";
    error = Error::NoError;
    directory = event->mimeData()->urls()[0].toLocalFile();
    print(directory);

    QDir qDirInput(directory + "/input");
    qDirInput.setFilter(QDir::Files);

    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    QElapsedTimer time;
    time.start();
    auto *progress = new QProgressDialog("", "Cancel", 0, 5, this);
    progress->setWindowTitle("Processing the data...");
    progress->setWindowModality(Qt::WindowModal);
    progress->setMinimumDuration(0);
    progress->setFixedSize(300, 150);
    progress->setLabelText("Reading wav file...");
    progress->setValue(0);
    QApplication::processEvents();
    audioFile = new AudioFile<double>();
    bool loadedOK = audioFile->load(directory.toStdString());
    if (loadedOK) {
        reset();
        int n = static_cast<int>(audioFile->samples[0].size());
        print(n);
        qSlider->setRange(0, n);
        progress->setValue(1);
        if (progress->wasCanceled()) {
            error = Error::UserCanseledImportAudioFile;
            qCustomPlotTime1->clearAll();
            return;
        }
        t.resize(n);
        u.resize(n);

        double fs = audioFile->getSampleRate();
        double T = 1.0/audioFile->getSampleRate();

        for (auto i = 0; i < n; ++i) {
            t[i] = (static_cast<double>(i))/fs;
            u[i] = audioFile->samples[0][i];
        }

        delete audioFile;

        progress->setLabelText("Algorithms initialization ...");
        QApplication::processEvents();

        boost::math::cubic_b_spline<double> spline(u.begin(), u.end(), t[0], t[1] - t[0], 0, 0);

        std::thread thr1(&Algorithm1::Init, algorithm1, std::ref(spline), t, u);
        std::thread thr2(&Algorithm2::Init, algorithm2, std::ref(spline), t, u);
        std::thread thr3(&Algorithm3::Init, algorithm3, std::ref(spline), t, u);
        thr1.join();
        progress->setValue(2);
        if (progress->wasCanceled()) {
            error = Error::UserCanseledImportAudioFile;
        }
        thr2.join();
        if (progress->wasCanceled() || error == Error::UserCanseledImportAudioFile) {
            error = Error::UserCanseledImportAudioFile;
            qCustomPlotTime1->clearAll();
            return;
        } else {
            progress->setValue(3);
        }
        thr3.join();
        if (progress->wasCanceled() || error == Error::UserCanseledImportAudioFile) {
            error = Error::UserCanseledImportAudioFile;
            qCustomPlotTime1->clearAll();
            return;
        } else {
            progress->setValue(4);
        }

        progress->setLabelText("Plotting the data...");
        QApplication::processEvents();
        if (progress->wasCanceled()) {
            error = Error::UserCanseledImportAudioFile;
            qCustomPlotTime1->clearAll();
            return;
        }

        if (!qCustomPlotTime1->graphCount()) {
            qCustomPlotTime1->addGraph();
        } else {
            qCustomPlotTime1->clearAt(0);
        }
        qCustomPlotTime1->graph(0)->setData(t, u);
        qCustomPlotTime1->rescaleAxes(true);
//        qCustomPlotTime1->replot(QCustomPlot::rpQueuedReplot);
        qCustomPlotTime1->replot();

//        qCustomPlotFreq2->graph(0)->setData(algorithm1->t,algorithm1->u);
//        qCustomPlotFreq2->addGraph();
//        qCustomPlotFreq2->graph(1)->setData(algorithm2->t,algorithm2->u);
//        qCustomPlotFreq2->addGraph();
//        qCustomPlotFreq2->graph(2)->setData(algorithm3->t,algorithm3->u);
//        qCustomPlotFreq2->rescaleAxes(true);
//        qCustomPlotFreq2->replot();
        progress->setValue(5);
    } else {
        error = Error::FileNameError;
        delete audioFile;
    }
    delete progress;
    print(error);
    end = std::chrono::system_clock::now();
    qDebug("Time elapsed: %lld ms", time.elapsed());
    qDebug("Time elapsed: %lld ms", std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count());
    qDebug() << "dropEvent: exit";
}

void
MainWindow::dragMoveEvent(QDragMoveEvent *event) {
    event->accept();
}

void
MainWindow::dragLeaveEvent(QDragLeaveEvent *event) {
    event->accept();
}

void
MainWindow::fft(QVector<double> &t_t, QVector<double> &u_t) {
    qDebug() << "fft: enter";
    print(t_t.size());
    if (t_t.size() > 100000) return;
    if (qCheckBoxFFT->isChecked()) {
        QElapsedTimer time;
        time.start();
        auto out_in = FFT<qVect_1d, qVect_1c>::rms_fft(t_t, u_t);
        qVect_1d f_in(out_in.first.size() / 2);
        qVect_1d s_in(out_in.first.size() / 2);
        for (int i = 0; i < out_in.first.size() / 2; i++) {
            f_in[i] = out_in.first[i];
            s_in[i] = sqrt(pow(out_in.second[i].real(), 2) + pow(out_in.second[i].imag(), 2));
        }
        qDebug("Time elapsed: %lld ms", time.elapsed());
        time.restart();
        qCustomPlotFreq2->graph()->setData(f_in, s_in);
        if (qCustomPlotFreq2->xRange_p.second == 0)
            qCustomPlotFreq2->rescaleAxes(true);
        qCustomPlotFreq2->replot(QCustomPlot::rpQueuedReplot);
//        qCustomPlotFreq2->replot();
        qDebug("Time elapsed: %lld ms", time.elapsed());
    } else {
        qCustomPlotFreq2->graph()->data().data()->clear();
        qCustomPlotFreq2->replot();
    }
}

void
MainWindow::decodeFrame() {
    qDebug() << "decodeFrame(): enter";
    if (t.empty()) return;

    double epsilon = t[1] - t[0];
    for (auto i = 0; i < t.size(); i++) {
        if (abs(t[i] - qCustomPlotTime1->xAxis->range().lower) <= epsilon) {
            n_min = i;
            break;
        }
    }
    for (auto i = n_min; i < t.size(); i++) {
        if (abs(t[i] - qCustomPlotTime1->xAxis->range().upper) <= epsilon) {
            n_max = i;
            break;
        }
    }
    int n = n_max - n_min;
    t_frame.resize(n);
    u_frame.resize(n);
    for (auto i = 0; i < n; i++) {
        t_frame[i] = t[n_min + i];
        u_frame[i] = u[n_min + i];
    }
    lastFunctionCalled = "decodeFrame()";
    qDebug() << "decodeFrame(): exit";
}

void
MainWindow::plotsUpdate() {
    qDebug() << "plotsUpdate(): enter";
    if (t.empty()) return;

    double epsilon = t[1] - t[0];
    n_min = 0;
    n_max = 0;
    auto lower = qCustomPlotTime1->xAxis->range().lower;
    auto upper = qCustomPlotTime1->xAxis->range().upper;
//    print(qCustomPlotTime1->xAxis->range().lower);
//    print(qCustomPlotTime1->xAxis->range().upper);
//    auto lower = qCustomPlotTime1->xRange_p2.first;
//    auto upper = qCustomPlotTime1->xRange_p2.second;
    print(lower);
    print(upper);
    for (auto i = 0; i < t.size(); i++) {
        if (abs(t[i] - lower) <= epsilon) {
            n_min = i;
            break;
        }
    }
    for (auto i = n_min; i < t.size(); i++) {
        if (abs(t[i] - upper) <= epsilon) {
            n_max = i;
            break;
        }
    }
    print(n_min);
    print(n_max);
    print(n_max - n_min);
    if (n_min) {
        qSlider->setValue(n_min);
        qSlider->setPageStep(n_max - n_min);
        auto i0 = qSlider->value();
        auto step = qSlider->pageStep();
        if (t_frame.size() != step) {
            t_frame.resize(step);
            u_frame.resize(step);
        }
        for (auto i = 0; i < step; i++) {
            t_frame[i] = t[i0 + i];
            u_frame[i] = u[i0 + i];
        }
        qCustomPlotTime1->graph(1)->data().data()->clear();
        qCustomPlotTime1->graph(1)->setData(t_frame, u_frame);
        qCustomPlotTime1->graph(1)->setPen(QColor(Qt::darkBlue));
//        qCustomPlotTime1->rescaleAxes(true);
        qCustomPlotTime1->replot(QCustomPlot::rpQueuedReplot);

        qCustomPlotTime2->graph(0)->setData(t_frame, u_frame);
        qCustomPlotTime2->graph(0)->setPen(QColor(Qt::darkBlue));
        qCustomPlotTime2->rescaleAxes(true);
        qCustomPlotTime2->replot(QCustomPlot::rpQueuedReplot);
    } else {
//        qSlider->setValue(0);
//        qSlider->setPageStep(1);
    }
    lastFunctionCalled = "plotsUpdate()";
    qDebug() << "plotsUpdate: exit";
}

void
MainWindow::qSliderReleased() {
    qDebug() << "qSliderReleased(): enter";
    print(qSlider->value());
    if (t.empty()) return;

    auto step = qSlider->pageStep();
    print(step);
    auto i0 = qSlider->value();
    if (i0 + step > t.size()) {
        step = t.size() - i0;
    }
    if (t_frame.size() != step) {
        t_frame.resize(step);
        u_frame.resize(step);
    }
    for (auto i = 0; i < step; i++) {
        t_frame[i] = t[i0 + i];
        u_frame[i] = u[i0 + i];
    }
    print(i0);

    qCustomPlotTime1->graph(1)->data().data()->clear();
    qCustomPlotTime1->graph(1)->setData(t_frame, u_frame);
    qCustomPlotTime1->graph(1)->setPen(QColor(Qt::darkBlue));
//    qCustomPlotTime1->rescaleAxes(true);
    qCustomPlotTime1->replot(QCustomPlot::rpQueuedReplot);

    qCustomPlotTime2->graph(0)->setData(t_frame, u_frame);
    qCustomPlotTime2->graph(0)->setPen(QColor(Qt::darkBlue));
    qCustomPlotTime2->rescaleAxes(true);
    qCustomPlotTime2->replot(QCustomPlot::rpQueuedReplot);

    fft(t_frame, u_frame);
    lastFunctionCalled = "qSliderReleased()";
    qDebug() << "qSliderReleased(): exit";
}

void
MainWindow::reset(){
    // slider reset
    qSlider->setValue(0);
    qSlider->setPageStep(1);
    // graphics reset
    qCustomPlotTime1->rescaleAxes(true);
    qCustomPlotTime1->clearAt(1);
    qCustomPlotTime2->clearAll();
    // data reset
    t_frame.clear();
    u_frame.clear();
}